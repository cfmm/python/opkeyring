#!/usr/bin/env python

import io

import setuptools

with io.open('README.md', encoding='utf-8') as readme:
    long_description = readme.read()

name = 'opkeyring'
description = '1Password keyring backend implementation'

params = dict(
    name=name,
    author="L. Martyn Klassen",
    author_email="lmklassen@gmail.com",
    version="0.2",
    description=description or name,
    long_description=long_description,
    url="https://gitlab.com/mklassen/" + name,
    packages=setuptools.find_packages(exclude=['tests']),
    python_requires='>=3.5',
    install_requires=[
        "keyring>=23.9.3",
        "cryptography>=38.0.1",
        "onepasswordconnectsdk>=1.2.0",
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
    entry_points={
        'keyring.backends': [
            'op = opkeyring.op',
        ],
    },
)
if __name__ == '__main__':
    setuptools.setup(**params)
