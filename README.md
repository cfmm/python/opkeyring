Read-only backend for keyring to access 1Password vault in OPVault format 

For 1Password account lookups require `op` command to be installed on the user's path.
https://support.1password.com/command-line-getting-started/

Unittesting

```
gitlab-runner exec docker --cache-dir /cache --docker-volumes `pwd`/ci:/cache test-3.9
```
