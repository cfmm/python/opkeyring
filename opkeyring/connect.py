import onepasswordconnectsdk
import pickle
import atexit
from cryptography.fernet import Fernet, InvalidToken
from onepasswordconnectsdk.models import Item, Field
from onepasswordconnectsdk.client import HTTPError
from threading import Thread


class FieldNotFound(Exception):
    pass


class ItemId:
    def __init__(self, item_id, vault_id):
        self.item_id = item_id
        self.vault_id = vault_id

    def __hash__(self):
        return f"{self.item_id}:{self.vault_id}".__hash__()

    def __eq__(self, other: "ItemId"):
        return self.item_id == other.item_id and self.vault_id == other.vault_id


class RecordId:
    def __init__(self, title, username):
        self.title = title
        self.username = username

    def __hash__(self):
        return f"{self.title}:{self.username}".__hash__()

    def __eq__(self, other: "RecordId"):
        return self.title == other.title and self.username == other.username


class Record:
    def __init__(self, value):
        self.value = value


class Interface:
    def __init__(self, url, token, vault, cached=False):
        self.client = onepasswordconnectsdk.new_client(url, token)
        self.vault_id = self.client.get_vault_by_title(vault).id
        if cached:
            self._mapper = dict()
            self._records = dict()
        else:
            self._mapper = None
            self._records = None

    @staticmethod
    def _get_field(fields, username):
        get_next = False
        for field in fields:
            if field.purpose == "USERNAME" and field.value == username:
                get_next = True
            elif get_next and field.purpose == "PASSWORD":
                return field

    @staticmethod
    def _get_value(fields, username):
        field = Interface._get_field(fields, username)
        if field is not None:
            return Record(field.value)

    def _lookup(self, item: RecordId):
        try:
            value: Item = self.client.get_item_by_title(item.title, self.vault_id)
        except HTTPError:
            raise KeyError

        record = self._get_value(value.fields, item.username)
        if record is None:
            raise KeyError

        key = ItemId(value.id, self.vault_id)
        try:
            self._records[key] = record
            self._mapper[item] = key
        except (TypeError, KeyError):
            pass

        return key, record

    def _get_item(self, item):
        try:
            key = self._mapper[item]
            record = self._records[key]
        except (TypeError, KeyError):
            key, record = self._lookup(item)
        return key, record

    def __getitem__(self, item):
        _, record = self._get_item(item)
        return record.value

    def delete(self, item):
        key, _ = self._get_item(item)
        self.client.delete_item(key.item_id, key.vault_id)
        try:
            self._mapper.pop(item)
            self._records.pop(key)
        except (TypeError, KeyError):
            pass

    def update(self, item, password):
        try:
            key, record = self._get_item(item)
            if record.value != password:
                record.value = password
                value: Item = self.client.get_item_by_id(key.item_id, key.vault_id)
                field = self._get_field(value.fields, item.username)
                if field is None:
                    raise FieldNotFound
                field.value = password
                self.client.update_item(key.item_id, key.vault_id, item)
        except KeyError:
            value = Item(title=item.title,
                         category="LOGIN",
                         tags=["keyring"],
                         fields=[Field(purpose="USERNAME", value=item.username),
                                 Field(purpose="PASSWORD", value=password)])
            value = self.client.create_item(self.vault_id, value)

            key = ItemId(value.id, self.vault_id)
            try:
                self._records[key] = Record(field=password)
                self._mapper[item] = ItemId(value.id, self.vault_id)
            except (TypeError, KeyError):
                pass


class InterfaceStored(Interface):
    def __init__(self, *args, storage=None, key=None, refresh=True, **kwargs):
        # Must use cache if using storage
        if storage is not None and key is not None:
            kwargs["cached"] = True

        super().__init__(*args, **kwargs)
        self.cache = storage
        self.key = key
        self.thread = None
        if self.cache is not None:
            self.thread = Thread(target=self._load_cache, args=(refresh,))
            self.thread.start()
            atexit.register(self._save_cache)

    def _load_cache(self, refresh=True):
        try:
            with open(self.cache, "rb") as fp:
                data = fp.read()
        except FileNotFoundError:
            return

        if self.key is not None:
            # Ignore the cache if the key is not correct
            try:
                data = Fernet(self.key).decrypt(data)
            except InvalidToken:
                return

        try:
            _mapper, _records = pickle.loads(data)
        except pickle.PickleError:
            return

        if not refresh:
            for item_id, record_id in _mapper.items():
                if record_id in self._records:
                    continue
                self._mapper[item_id] = record_id
                self._records[record_id] = _records[record_id]

        for item_id, record_id in _mapper.items():
            # Skip the record if it has already been read
            # There may still be race condition in which the item is looked up twice, e.g. a query is currently in
            # progress or start before this record is populated from cache.
            if refresh and (record_id in self._records):
                continue

            try:
                item: Item = self.client.get_item_by_id(record_id.item_id, record_id.vault_id)
            except HTTPError:
                continue

            record = self._get_value(item.fields, item_id.username)

            if record:
                # Store record and then mapper, because lookup is mapper then record, and this ensures the record
                # will exist if the mapper lookup is successful
                self._records[record_id] = record
                self._mapper[item_id] = record_id

    def _save_cache(self):
        self.thread.join()

        data = pickle.dumps((self._mapper, self._records))
        if self.key:
            try:
                data = Fernet(self.key).encrypt(data)
            except InvalidToken:
                return

        with open(self.cache, "wb") as fp:
            fp.write(data)
