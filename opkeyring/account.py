from typing import Optional

from keyring.backend import KeyringBackend
from keyring.errors import PasswordSetError, PasswordDeleteError

from .connect import Interface, RecordId, InterfaceStored, FieldNotFound, HTTPError


class OPConnectKeyring(KeyringBackend):
    priority = 1

    def __init__(self, url, token, vault, cache=None, key=None):
        super().__init__()
        if cache is not None and key is not None:
            self.op = InterfaceStored(url, token, vault, storage=cache, key=key)
        else:
            self.op = Interface(url, token, vault, cached=True)

    def get_password(self, service: str, username: str) -> Optional[str]:
        try:
            return self.op[RecordId(service, username)]
        except KeyError:
            pass

        return None

    def delete_password(self, service, username):
        try:
            self.op.delete(RecordId(service, username))
        except (HTTPError, Exception):
            raise PasswordDeleteError('Failed to delete password')

    def set_password(self, service, username, password):
        try:
            self.op.update(RecordId(service, username), password)
        except (FieldNotFound, HTTPError, Exception):
            raise PasswordSetError("Failed to update password")
