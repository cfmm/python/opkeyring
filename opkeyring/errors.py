class OPException(Exception):
    pass


class OPVaultNotFound(OPException):
    pass


class OPProfileNotFound(OPException):
    pass


class OPItemNotFound(OPException):
    pass


class OPDecryptError(OPException):
    pass


class OPDecodeError(OPException):
    pass


class OPAccountAuthorization(OPException):
    pass
