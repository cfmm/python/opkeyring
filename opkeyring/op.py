import glob
import hashlib
import hmac
import json
import os
import struct
from base64 import decodebytes

from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from keyring.backend import KeyringBackend
from keyring.errors import PasswordSetError, PasswordDeleteError

from .errors import OPDecodeError, OPException, OPDecryptError, OPItemNotFound, OPProfileNotFound, OPVaultNotFound


class OP:
    def __init__(self, path, profile='default'):
        self._path = path
        self._profile = profile

        self._master_key = self._master_mac_key = None
        self._overview_key = self._overview_mac_key = None

        self._items = None
        self._item_index = []

        self._validate_vault()
        self._validate_profile()

    def __del__(self):
        self.lock()

    def get_items(self):
        return self._item_index

    def _validate_vault(self):
        if not os.path.isdir(self._path) and not os.path.islink(self._path):
            raise OPVaultNotFound('Vault not found in {0}'.format(self._path))

        if not os.access(self._path, os.R_OK):
            raise OPVaultNotFound('Vault not readable {0}'.format(self._path))

        return True

    def _validate_profile(self):
        profile_path = '{0}/{1}/profile.js'.format(self._path, self._profile)
        if not os.path.isfile(profile_path):
            raise OPProfileNotFound('Profile not readable {0}'.format(profile_path))

        try:
            with open(profile_path, 'r') as profile_file:
                profile_content = profile_file.read().strip()
        except (IOError, AttributeError) as e:
            raise OPProfileNotFound('Cannot open profile file {0}'.format(profile_path))

        if not profile_content.startswith('var profile=') or not profile_content.endswith(';'):
            raise OPProfileNotFound('Invalid syntax in {0}'.format(profile_path))

        try:
            self._profile_json = json.loads(profile_content[12:-1])
        except ValueError as e:
            raise OPProfileNotFound('Cannot parse profile {0}'.format(str(e)))

        return True

    def unlock(self, master_password):
        salt = bytes(decodebytes(self._profile_json['salt'].encode()))
        iterations = self._profile_json['iterations']

        key, mac_key = self._derive_keys(master_password.encode(), salt, iterations)

        try:
            self._master_key, self._master_mac_key = self.master_keys(key, mac_key)
            self._overview_key, self._overview_mac_key = self.overview_keys(key, mac_key)
        except OPException as e:
            raise OPDecryptError('Incorrect password: "{0}"'.format(str(e)))

        return True

    def lock(self):
        self._master_key = self._master_mac_key = None

        return True

    def is_unlocked(self):
        return bool(self._master_key and self._overview_key)

    @staticmethod
    def _derive_keys(master_password, salt, iterations):
        derived_key = hashlib.pbkdf2_hmac('sha512', master_password, salt, iterations)
        key = derived_key[:32]
        mac_key = derived_key[32:64]

        return key, mac_key

    def master_keys(self, derived_key, derived_mac_key):
        encrypted = decodebytes(self._profile_json['masterKey'].encode())

        return self.decrypt_keys(encrypted, derived_key, derived_mac_key)

    def overview_keys(self, derived_key, derived_mac_key):
        encrypted = decodebytes(self._profile_json['overviewKey'].encode())

        return self.decrypt_keys(encrypted, derived_key, derived_mac_key)

    def decrypt_keys(self, encrypted_key, derived_key, derived_mac_key):
        key_base = self.decrypt_opdata(encrypted_key, derived_key, derived_mac_key)

        keys = hashlib.sha512(bytes(key_base))
        digest = keys.digest()

        key_from_digest = digest[:32]
        hmac_from_digest = digest[32:64]

        return key_from_digest, hmac_from_digest

    def decrypt_opdata(self, cipher_text, cipher_key, cipher_mac_key):
        key_data = cipher_text[:-32]
        mac_data = cipher_text[-32:]

        self.check_hmac(key_data, cipher_mac_key, mac_data)

        plaintext = self.decrypt_data(cipher_key, key_data[16:32], key_data[32:])
        plaintext_size = int(struct.unpack('Q', key_data[8:16])[0])

        plaintext_start = plaintext_size * -1
        opdata = plaintext[plaintext_start:]

        return opdata

    @staticmethod
    def check_hmac(data, hmac_key, desired_hmac):
        computed_hmac = hmac.new(hmac_key, msg=data, digestmod=hashlib.sha256).digest()

        if bytes(computed_hmac) != bytes(desired_hmac):
            raise OPDecodeError('Error checking HMAC')

        return True

    def load_items(self, exclude_trashed=False):
        file_glob = os.path.join(self._path, self._profile, 'band_*.js')

        self._items = {}
        for item in glob.glob(file_glob):
            with open(item, 'r') as f:
                content = f.read()[3:-2]
                try:
                    band = json.loads(content)
                    self._items.update(band)
                except ValueError:
                    pass

        self._item_index = []
        for uuid, item in self._items.items():
            overview = self.item_overview(item)
            if 'title' in overview:
                if exclude_trashed and item.get('trashed', False):
                    continue
                self._item_index.append(overview)

        return self._items

    def item_keys(self, item):
        item_key = decodebytes(item['k'].encode())
        key_data = item_key[:-32]
        key_hmac = item_key[-32:]

        self.check_hmac(key_data, self._master_mac_key, key_hmac)
        plaintext = self.decrypt_data(self._master_key, key_data[0:16], key_data[16:])

        decrypted_key = plaintext[0:32]
        decrypted_hmac = plaintext[32:64]

        return decrypted_key, decrypted_hmac

    def load_data(self, data, key, mac_key, uuid):
        try:
            data = self.decrypt_opdata(data, key, mac_key)
            data = json.loads(data)

        except OPException as e:
            raise OPDecryptError('Cannot decrypt item: {0}, error: "{1}"'.format(uuid, str(e)))

        except ValueError as e:
            raise OPDecodeError('Cannot parse item: {0}, error: "{1}"'.format(uuid, str(e)))

        return data

    def item_overview(self, item):
        overview_data = decodebytes(item['o'].encode())
        item_data = self.load_data(overview_data, self._overview_key, self._overview_mac_key, item['uuid'])
        item_data.update({u'uuid': item['uuid']})
        return item_data

    def item_detail(self, item):
        data = decodebytes(item['d'].encode())
        item_key, item_mac_key = self.item_keys(item)
        item_detail = self.load_data(data, item_key, item_mac_key, item['uuid'])
        return item_detail

    def get_item(self, title, ainfo=None, uuid=None):
        try:
            # Get the first item that matches all inputs
            overview = next(x for x in self._item_index
                            if x['title'] == title and
                            ((ainfo is None) or x['ainfo'] == ainfo) and
                            ((uuid is None) or x['uuid'] == uuid))
            item = self._items[overview['uuid']]
        except (StopIteration, KeyError):
            raise OPItemNotFound('Item with title {0} does not exists'.format(title))

        details = self.item_detail(item)

        return overview, details

    @staticmethod
    def decrypt_data(key, iv, data):
        cipher = Cipher(algorithms.AES(key), modes.CBC(iv))
        decryptor = cipher.decryptor()
        return decryptor.update(data) + decryptor.finalize()


class OPKeyring(KeyringBackend):
    priority = 1

    def __init__(self, password, path, profile='default', *args, **kwargs):
        super(OPKeyring, self).__init__(*args, **kwargs)
        self.vault = OP(path, profile)
        self.vault.unlock(master_password=password)
        self.vault.load_items()

    @staticmethod
    def parse(details, username):
        """
        1Password stores a lot more than just username and password. service and username are really insufficient for
        looking up data in 1Password. There may be multiple items with the same lookup information in 1Password.
        Assumption is made that the password is the first password type field in the section that contains field of
        username type with requested username.
        """
        if 'fields' in details:
            if any(not username or (field.get('designation') == 'username' and field.get('value') == username)
                   for field in details['fields']):
                # Return the contents of the first password field
                try:
                    return next(field.get('value') for field in details['fields']
                                if field.get('designation') == 'password')
                except StopIteration:
                    pass

        try:
            # Get the section with username equal to requested value
            section = next(section for section in details.get('sections', [details])
                           if any(field.get('n', field.get('designation')) in ('username', 'pop_username') and
                                  field.get('v', field.get('value')) == username
                                  for field in section.get('fields', [])))

            # Return the contents of the first password field in the requested section
            return next(field.get('v', field.get('value')) for field in section.get('fields', [])
                        if field.get('n', field.get('designation')) in ('password', 'pop_password'))
        except StopIteration:
            pass

            # Get the section with a concealed field with a title that matches username
        for section in details.get('sections', []):
            for field in section.get('fields', []):
                if (not username or field.get('t') == username) and field.get('k') == 'concealed':
                    return field.get('v')

        return None

    def get_password(self, service, username):
        try:
            try:
                overview, details = self.vault.get_item(**service)
            except TypeError:
                overview, details = self.vault.get_item(service, username)
        except OPItemNotFound:
            return None

        return self.parse(details, username)

    def delete_password(self, service, username):
        raise PasswordDeleteError('Keyring is read-only')

    def set_password(self, service, username, password):
        raise PasswordSetError('Keyring is read-only')
