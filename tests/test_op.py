import inspect
import os
import unittest

import keyring
import time

from opkeyring.op import OPKeyring

DATA_PATH = os.path.dirname(inspect.getabsfile(inspect.currentframe()))


class OPTest(unittest.TestCase):

    def test_franky(self):
        keyring.set_keyring(OPKeyring('franky', os.path.join(DATA_PATH, 'franky.opvault')))
        data = keyring.get_password('YouTube', 'franky')
        # There are two entries only distinguishable by UUID
        self.assertIn(data, ['abcdef', '12345'])

        data = keyring.get_password('YouTube', 'freddy')
        self.assertEqual('abcdef', data)

        data = keyring.get_password('server.example.com', 'root')
        self.assertEqual('123456', data)

        data = keyring.get_password('Email Account', 'email_user')
        self.assertEqual('abcdef', data)

    def test_server(self):
        keyring.set_keyring(OPKeyring('franky', os.path.join(DATA_PATH, 'franky.opvault')))
        data = keyring.get_password({'title': 'server.example.com',
                                     'ainfo': 'root'},
                                    'console password')
        self.assertEqual('abcdef', data)

    def test_noname(self):
        keyring.set_keyring(OPKeyring('franky', os.path.join(DATA_PATH, 'franky.opvault')))
        data = keyring.get_password('Noname', None)
        self.assertEqual('abcdef', data)

    def test_freddy(self):
        keyring.set_keyring(OPKeyring('freddy', os.path.join(DATA_PATH, 'onepassword_data')))
        data = keyring.get_password('YouTube', 'wendy@appleseed.com')
        self.assertEqual('snaip5uc5keds7as5ocs', data)
