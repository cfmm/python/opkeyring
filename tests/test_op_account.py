import unittest

import keyring
import yaml

from opkeyring import OPConnectKeyring


class OPAccountTestCase(unittest.TestCase):
    def test_lookup(self):
        with open('account.yml') as fp:
            config = yaml.safe_load(fp)

        config["cache"] = "cache.pkl"
        config["key"] = b'I_TdW9NN2cPE15WTfxzD4uTV7Nfl7GTT-Sg5kwvhQJU='
        keyring.set_keyring(OPConnectKeyring(**config))
        data = keyring.get_password("Test", "test")
        self.assertEqual(data, 'wfXFs7YbHnHYhsK4lQjs')


if __name__ == '__main__':
    unittest.main()
